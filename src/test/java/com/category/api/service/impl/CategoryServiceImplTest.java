package com.category.api.service.impl;

import com.category.api.dto.*;
import com.category.api.exception.CategoryNotFoundException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CategoryServiceImplTest {

    private final static Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    CategoryServiceImpl categoryService;

    @Test
    @Order(1)
    void createCategory() {
        CreateCategoryRequest categoryRequest = CreateCategoryRequest.builder()
                .name("Test Create Category Service")
                .description("This is test service create category description")
                .build();
        GetCategoryResponse getCategoryResponse = categoryService.createCategory(categoryRequest);

        assertNotNull(getCategoryResponse.getId());
        assertNotNull(getCategoryResponse.getCreatedAt());

        log.info("ID = {}", getCategoryResponse.getId());
        log.info("Name = {}", getCategoryResponse.getName());
        log.info("Created At = {}", getCategoryResponse.getCreatedAt());
        log.info("Updated At = {}", getCategoryResponse.getUpdatedAt());
    }

    @Test
    @Order(2)
    void getCategoryById() throws CategoryNotFoundException {
        String categoryId = "laptop";
        GetCategoryResponse getCategoryResponse = categoryService.getCategoryById(categoryId);

        assertNotNull(getCategoryResponse);
        assertEquals(categoryId, getCategoryResponse.getId());

        log.info("ID = {}", getCategoryResponse.getId());
        log.info("Created At = {}", getCategoryResponse.getCreatedAt());
        log.info("Updated At = {}", getCategoryResponse.getUpdatedAt());
    }

    @Test
    @Order(3)
    void updateCategory() throws CategoryNotFoundException {
        String categoryId = "cctv";
        UpdateCategoryRequest updateCategoryRequest = UpdateCategoryRequest.builder()
                .name("Test Update Service")
                .description("This is test update service category description")
                .build();
        GetCategoryResponse getCategoryResponse = categoryService.updateCategory(categoryId, updateCategoryRequest);

        assertEquals(categoryId, getCategoryResponse.getId());
        assertNotNull(getCategoryResponse.getUpdatedAt());
        assertNotEquals(getCategoryResponse.getCreatedAt(), getCategoryResponse.getUpdatedAt());

        log.info("Name = {}", getCategoryResponse.getName());
        log.info("Description = {}", getCategoryResponse.getDescription());
        log.info("Created At = {}", getCategoryResponse.getCreatedAt());
        log.info("Updated At = {}", getCategoryResponse.getUpdatedAt());
    }

    @Test
    @Order(4)
    void getAllCategories() {
        int totalSampleDataCategories = 12; // 12 setelah ditambah method create category
        GetAllCategoryRequest getAllCategoryRequest = new GetAllCategoryRequest();
        getAllCategoryRequest.setPageSize(5);
        getAllCategoryRequest.setPageNo(0);
        getAllCategoryRequest.setSortBy("name");
        getAllCategoryRequest.setSortDir("asc");
        GetAllCategoryResponse getAllCategoryResponse = categoryService.getAllCategories(getAllCategoryRequest);

        assertEquals(getAllCategoryRequest.getPageSize(), getAllCategoryResponse.getPageSize());
        assertEquals(getAllCategoryRequest.getPageNo(), getAllCategoryResponse.getPageNo());
        assertEquals(totalSampleDataCategories, getAllCategoryResponse.getTotalElements());

        log.info("Page No = {}", getAllCategoryResponse.getPageNo());
        log.info("Page Size = {}", getAllCategoryResponse.getPageSize());
        log.info("Total Category = {}", getAllCategoryResponse.getTotalElements());
        log.info("Total Page = {}", getAllCategoryResponse.getTotalPages());
    }

    @Test
    @Order(5)
    void deleteCategory() throws CategoryNotFoundException {
        String categoryId = "disk";
        categoryService.deleteCategory(categoryId);
        assertThrows(CategoryNotFoundException.class, () -> {
            GetCategoryResponse getCategoryResponse = categoryService.getCategoryById(categoryId);
        });
    }

    @Test
    @Order(6)
    void getCategoryByName() throws CategoryNotFoundException {
        String name = "Laptop";
        GetCategoryResponse getCategoryResponse = categoryService.getCategoryByName(name);
        assertEquals(name, getCategoryResponse.getName());
        log.info("Category Name = {}", getCategoryResponse.getName());
    }

    @Test
    @Order(7)
    void getCategoryByNameContaining() {
        String name = "mo";
        List<GetCategoryResponse> getCategoryResponses = categoryService.getCategoryByNameContaining(name);
        assertEquals(2, getCategoryResponses.size());
        for (GetCategoryResponse category : getCategoryResponses) {
            log.info("Name = {}", category.getName());
            log.info("==========");
        }
    }

}