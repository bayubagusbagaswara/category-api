package com.category.api.controller;

import com.category.api.dto.*;
import com.category.api.exception.CategoryNotFoundException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CategoryControllerTest {

    private final static Logger log = LoggerFactory.getLogger(CategoryControllerTest.class);

    @Autowired
    CategoryController categoryController;

    @Test
    @Order(1)
    void createCategory() {
        CreateCategoryRequest categoryRequest = CreateCategoryRequest.builder()
                .name("Test Category Controller")
                .description("Test Category Controller description")
                .build();
        WebResponse<GetCategoryResponse> getCategoryResponseWebResponse = categoryController.createCategory(categoryRequest);

        assertEquals(HttpStatus.CREATED.value(), getCategoryResponseWebResponse.getCode());
        assertEquals(HttpStatus.CREATED.getReasonPhrase(), getCategoryResponseWebResponse.getStatus());
        assertNotNull(getCategoryResponseWebResponse.getData().getId());
        assertNotNull(getCategoryResponseWebResponse.getData().getCreatedAt());
        assertEquals(categoryRequest.getName(), getCategoryResponseWebResponse.getData().getName());

        log.info("Code = {}", getCategoryResponseWebResponse.getCode());
        log.info("Status = {}", getCategoryResponseWebResponse.getStatus());
        log.info("Data ID = {}", getCategoryResponseWebResponse.getData().getId());
        log.info("Data Name = {}", getCategoryResponseWebResponse.getData().getName());
        log.info("Data Created At = {}", getCategoryResponseWebResponse.getData().getCreatedAt());
    }

    @Test
    @Order(2)
    void getCategoryById() throws CategoryNotFoundException {
        String categoryId = "laptop";
        WebResponse<GetCategoryResponse> getCategoryResponseWebResponse = categoryController.getCategoryById(categoryId);

        assertEquals(HttpStatus.OK.value(), getCategoryResponseWebResponse.getCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), getCategoryResponseWebResponse.getStatus());
        assertEquals(categoryId, getCategoryResponseWebResponse.getData().getId());

        log.info("Code = {}", getCategoryResponseWebResponse.getCode());
        log.info("Status = {}", getCategoryResponseWebResponse.getStatus());
        log.info("Data ID = {}", getCategoryResponseWebResponse.getData().getId());
    }

    @Test
    @Order(3)
    void updateCategory() throws CategoryNotFoundException {
        String categoryId = "cctv";
        UpdateCategoryRequest updateCategoryRequest = UpdateCategoryRequest.builder()
                .name("Controller Test")
                .description("Controller test description")
                .build();
        WebResponse<GetCategoryResponse> getCategoryResponseWebResponse = categoryController.updateCategory(categoryId, updateCategoryRequest);
        assertEquals(HttpStatus.OK.value(), getCategoryResponseWebResponse.getCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), getCategoryResponseWebResponse.getStatus());
        assertEquals(categoryId, getCategoryResponseWebResponse.getData().getId());
        assertEquals(updateCategoryRequest.getName(), getCategoryResponseWebResponse.getData().getName());
        assertNotNull(getCategoryResponseWebResponse.getData().getUpdatedAt());
        assertNotEquals(getCategoryResponseWebResponse.getData().getCreatedAt(), getCategoryResponseWebResponse.getData().getUpdatedAt());

        log.info("Code = {}", getCategoryResponseWebResponse.getCode());
        log.info("Status = {}", getCategoryResponseWebResponse.getStatus());
        log.info("Data ID = {}", getCategoryResponseWebResponse.getData().getId());
        log.info("Data Name = {}", getCategoryResponseWebResponse.getData().getName());
    }

    @Test
    @Order(4)
    void getAllCategories() {
        int pageNo = 0;
        int pageSize = 5;
        String sortBy = "name";
        String sorDir = "asc";
        int totalSampleData = 12;

        WebResponse<GetAllCategoryResponse> getAllCategoryResponseWebResponse = categoryController.getAllCategories(pageNo, pageSize, sortBy, sorDir);
        assertEquals(HttpStatus.OK.value(), getAllCategoryResponseWebResponse.getCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), getAllCategoryResponseWebResponse.getStatus());

        assertEquals(totalSampleData, getAllCategoryResponseWebResponse.getData().getTotalElements());
        assertEquals(pageNo, getAllCategoryResponseWebResponse.getData().getPageNo());
        assertEquals(pageSize, getAllCategoryResponseWebResponse.getData().getPageSize());

        log.info("PageNo = {}", getAllCategoryResponseWebResponse.getData().getPageNo());
        log.info("PageSize = {}", getAllCategoryResponseWebResponse.getData().getPageSize());
        log.info("Total Data = {}", getAllCategoryResponseWebResponse.getData().getTotalElements());
        log.info("Total Page = {}", getAllCategoryResponseWebResponse.getData().getTotalPages());
    }

    @Test
    @Order(5)
    void deleteCategory() throws CategoryNotFoundException {
        String categoryId = "disk";
        WebResponse<String> stringWebResponse = categoryController.deleteCategory(categoryId);
        assertEquals(HttpStatus.OK.value(), stringWebResponse.getCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), stringWebResponse.getStatus());
        assertNull(stringWebResponse.getData());

        assertThrows(CategoryNotFoundException.class, () -> {
            WebResponse<GetCategoryResponse> getCategoryResponse = categoryController.getCategoryById(categoryId);
        });
    }

    @Test
    @Order(6)
    void getCategoryByName() throws CategoryNotFoundException {
        String name = "Laptop";
        WebResponse<GetCategoryResponse> getCategoryResponseWebResponse = categoryController.getCategoryByName(name);

        assertEquals(HttpStatus.OK.value(), getCategoryResponseWebResponse.getCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), getCategoryResponseWebResponse.getStatus());
        assertEquals(name, getCategoryResponseWebResponse.getData().getName());

        log.info("Name = {}", getCategoryResponseWebResponse.getData().getName());
    }

    @Test
    @Order(7)
    void getCategoryByNameContaining() {
        String name = "mo";
        WebResponse<List<GetCategoryResponse>> getListWebResponse = categoryController.getCategoryByNameContaining(name);

        assertEquals(HttpStatus.OK.value(), getListWebResponse.getCode());
        assertEquals(HttpStatus.OK.getReasonPhrase(), getListWebResponse.getStatus());
        assertEquals(2, getListWebResponse.getData().size());

        for (GetCategoryResponse category : getListWebResponse.getData()) {
            log.info("Name = {}", category.getName());
            log.info("========");
        }
    }
}