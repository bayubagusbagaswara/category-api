
create table categories (
    id character varying(64) not null primary key default uuid_generate_v4(),
    name character varying(64) not null,
    description character varying(255) not null,
    status character varying(255) not null,
    created_at timestamp not null default now(),
    created_by character varying(255) not null,
    updated_at timestamp,
    updated_by character varying(255)
);

