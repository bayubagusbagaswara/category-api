-- ELECTRONIC (Telephone, AC, Television, Audio & Speaker, CCTV, Camera)
insert into categories (id, name, description, created_by, created_at, status)
values ('tv', 'Television', 'This is television description', 'Current User', current_timestamp, 'ACTIVE'),
        ('cctv', 'CCTV', 'This is CCTV description', 'Current User', current_timestamp, 'ACTIVE'),
        ('camera', 'Camera', 'This is camera description', 'Current User', current_timestamp, 'ACTIVE');

-- COMPUTER AND ACCESSORIES (Laptop, Hard disk, monitor, keyboard, mouse, router wifi)
insert into categories (id, name, description, created_by, created_at, status)
values ('laptop', 'Laptop', 'This is laptop description', 'Current User', current_timestamp, 'ACTIVE'),
        ('disk', 'Disk', 'This is disk description', 'Current User', current_timestamp, 'ACTIVE'),
        ('monitor', 'Monitor', 'This is monitor description', 'Current User', current_timestamp, 'ACTIVE'),
        ('printer', 'Printer', 'This is printer description', 'Current User', current_timestamp, 'ACTIVE'),
        ('keyboard', 'Keyboard', 'This is keyboard description', 'Current User', current_timestamp, 'ACTIVE'),
        ('mouse', 'Mouse', 'This is mouse description', 'Current User', current_timestamp, 'ACTIVE');

-- HAND PHONE AND ACCESSORIES (Tablet, hand phone, power bank)
insert into categories (id, name, description, created_by, created_at, status)
values ('tablet', 'Tablet', 'This is tablet description', 'Current User', current_timestamp, 'ACTIVE'),
        ('phone', 'Hand Phone', 'This is hand phone description', 'Current User', current_timestamp, 'ACTIVE');
