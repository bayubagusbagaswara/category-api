package com.category.api.controller;

import com.category.api.dto.WebResponse;
import com.category.api.exception.CategoryNotFoundException;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorController {

    @ExceptionHandler(value = CategoryNotFoundException.class)
    public WebResponse<String> dataNotFoundHandler(CategoryNotFoundException categoryNotFoundException) {
        return WebResponse.<String>builder()
                .code(HttpStatus.NOT_FOUND.value())
                .status(HttpStatus.NOT_FOUND.getReasonPhrase())
                .data("Data not found")
                .build();
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public WebResponse<String> validatorHandler(ConstraintViolationException constraintViolationException) {
        return WebResponse.<String>builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .data(constraintViolationException.getMessage())
                .build();
    }
}