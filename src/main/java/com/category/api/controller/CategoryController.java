package com.category.api.controller;

import com.category.api.dto.*;
import com.category.api.exception.CategoryNotFoundException;
import com.category.api.service.CategoryService;
import com.category.api.utils.AppConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetCategoryResponse> createCategory(CreateCategoryRequest createCategoryRequest) {
        GetCategoryResponse getCategoryResponse = categoryService.createCategory(createCategoryRequest);
        return WebResponse.<GetCategoryResponse>builder()
                .code(HttpStatus.CREATED.value())
                .status(HttpStatus.CREATED.getReasonPhrase())
                .data(getCategoryResponse)
                .build();
    }

    @GetMapping(value = "/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetCategoryResponse> getCategoryById(@PathVariable("categoryId") String categoryId) throws CategoryNotFoundException {
        GetCategoryResponse getCategoryResponse = categoryService.getCategoryById(categoryId);
        return WebResponse.<GetCategoryResponse>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK.getReasonPhrase())
                .data(getCategoryResponse)
                .build();
    }

    @PutMapping(value = "/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetCategoryResponse> updateCategory(@PathVariable("categoryId") String categoryId, @RequestBody UpdateCategoryRequest updateProductRequest) throws CategoryNotFoundException {
        GetCategoryResponse getCategoryResponse = categoryService.updateCategory(categoryId, updateProductRequest);
        return WebResponse.<GetCategoryResponse>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK.getReasonPhrase())
                .data(getCategoryResponse)
                .build();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetAllCategoryResponse> getAllCategories(
            @RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) Integer pageNo,
            @RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) Integer pageSize,
            @RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
            @RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {

        GetAllCategoryRequest getAllCategoryRequest = new GetAllCategoryRequest();
        getAllCategoryRequest.setPageNo(pageNo);
        getAllCategoryRequest.setPageSize(pageSize);
        getAllCategoryRequest.setSortBy(sortBy);
        getAllCategoryRequest.setSortDir(sortDir);
        GetAllCategoryResponse responses = categoryService.getAllCategories(getAllCategoryRequest);
        return WebResponse.<GetAllCategoryResponse>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK.getReasonPhrase())
                .data(responses)
                .build();
    }

    @DeleteMapping(value = "/{categoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<String> deleteCategory(@PathVariable("categoryId") String categoryId) throws CategoryNotFoundException {
        categoryService.deleteCategory(categoryId);
        return WebResponse.<String>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK.getReasonPhrase())
                .data(null)
                .build();
    }


    @GetMapping(value = "/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<GetCategoryResponse> getCategoryByName(@PathVariable("name") String name) throws CategoryNotFoundException {
        GetCategoryResponse getCategoryResponses = categoryService.getCategoryByName(name);
        return WebResponse.<GetCategoryResponse>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK.getReasonPhrase())
                .data(getCategoryResponses)
                .build();
    }

    @GetMapping(value = "/name/containing/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WebResponse<List<GetCategoryResponse>> getCategoryByNameContaining(@PathVariable("name") String name) {
        List<GetCategoryResponse> getCategoryResponses = categoryService.getCategoryByNameContaining(name);
        return WebResponse.<List<GetCategoryResponse>>builder()
                .code(HttpStatus.OK.value())
                .status(HttpStatus.OK.getReasonPhrase())
                .data(getCategoryResponses)
                .build();
    }
}
