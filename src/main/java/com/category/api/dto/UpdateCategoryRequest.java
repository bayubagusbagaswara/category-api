package com.category.api.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpdateCategoryRequest {

    @NotBlank(message = "Name must not be blank")
    @Size(max = 100, message = "Name length max must be 100 characters")
    private String name;

    @NotBlank(message = "Description must not be blank")
    @Size(min = 10, max = 500, message = "Description length minimum must be 10 characters and maximum must be 500 characters")
    private String description;
}
