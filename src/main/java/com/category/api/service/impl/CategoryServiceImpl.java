package com.category.api.service.impl;

import com.category.api.dto.*;
import com.category.api.entity.Category;
import com.category.api.exception.CategoryNotFoundException;
import com.category.api.repository.CategoryRepository;
import com.category.api.service.CategoryService;
import com.category.api.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final ValidationUtil validationUtil;

    public CategoryServiceImpl(CategoryRepository categoryRepository, ValidationUtil validationUtil) {
        this.categoryRepository = categoryRepository;
        this.validationUtil = validationUtil;
    }

    @Override
    public GetCategoryResponse createCategory(CreateCategoryRequest createCategoryRequest) {
        validationUtil.validate(createCategoryRequest);
        Category category = new Category();
        category.setName(createCategoryRequest.getName());
        category.setDescription(createCategoryRequest.getDescription());
        category.setCreatedAt(LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)));
        categoryRepository.save(category);
        return mapCategoryToGetCategoryResponse(category);
    }

    @Override
    public GetCategoryResponse getCategoryById(String categoryId) throws CategoryNotFoundException {
        Category category = getCategory(categoryId);
        return mapCategoryToGetCategoryResponse(category);
    }

    @Override
    public GetAllCategoryResponse getAllCategories(GetAllCategoryRequest getAllCategoryRequest) {
        validationUtil.validate(getAllCategoryRequest);
        Integer pageNo = getAllCategoryRequest.getPageNo();
        Integer pageSize = getAllCategoryRequest.getPageSize();
        String sortBy = getAllCategoryRequest.getSortBy();
        String sortDir = getAllCategoryRequest.getSortDir();

        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<Category> categories = categoryRepository.findAll(pageable);
        List<Category> categoryList = categories.getContent();
        List<GetCategoryResponse> getCategoryResponseList = mapCategoryListToGetCategoryResponseList(categoryList);

        GetAllCategoryResponse categoryResponse = new GetAllCategoryResponse();
        categoryResponse.setCategoryResponses(getCategoryResponseList);
        categoryResponse.setPageNo(categories.getNumber());
        categoryResponse.setPageSize(categories.getSize());
        categoryResponse.setTotalElements(categories.getTotalElements());
        categoryResponse.setTotalPages(categories.getTotalPages());
        categoryResponse.setLast(categories.isLast());
        return categoryResponse;
    }

    @Override
    public GetCategoryResponse updateCategory(String categoryId, UpdateCategoryRequest updateCategoryRequest) throws CategoryNotFoundException {
        validationUtil.validate(updateCategoryRequest);
        Category category = getCategory(categoryId);
        category.setName(updateCategoryRequest.getName());
        category.setDescription(updateCategoryRequest.getDescription());
        category.setUpdatedAt(LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)));
        categoryRepository.save(category);
        return mapCategoryToGetCategoryResponse(category);
    }

    @Override
    public void deleteCategory(String categoryId) throws CategoryNotFoundException {
        Category category = getCategory(categoryId);
        categoryRepository.delete(category);
    }

    @Override
    public GetCategoryResponse getCategoryByName(String name) throws CategoryNotFoundException {
        Category category = categoryRepository.findByNameIgnoreCase(name).orElseThrow(() -> new CategoryNotFoundException("Category Name: [" +name+ "] not found"));
        return mapCategoryToGetCategoryResponse(category);
    }

    @Override
    public List<GetCategoryResponse> getCategoryByNameContaining(String name) {
        List<Category> categoryList = categoryRepository.findByNameContainingIgnoreCase(name);
        return mapCategoryListToGetCategoryResponseList(categoryList);
    }

    private GetCategoryResponse mapCategoryToGetCategoryResponse(Category category) {
        GetCategoryResponse getCategoryResponse = new GetCategoryResponse();
        getCategoryResponse.setId(category.getId());
        getCategoryResponse.setName(category.getName());
        getCategoryResponse.setDescription(category.getDescription());
        getCategoryResponse.setCreatedBy(category.getCreatedBy());
        getCategoryResponse.setCreatedAt(category.getCreatedAt());
        getCategoryResponse.setUpdatedBy(category.getUpdatedBy());
        getCategoryResponse.setUpdatedAt(category.getUpdatedAt());
        return getCategoryResponse;
    }

    private List<GetCategoryResponse> mapCategoryListToGetCategoryResponseList(List<Category> categoryList) {
        return categoryList.stream()
                .map((category) -> {
                    GetCategoryResponse getCategoryResponse = new GetCategoryResponse();
                    getCategoryResponse.setId(category.getId());
                    getCategoryResponse.setName(category.getName());
                    getCategoryResponse.setDescription(category.getDescription());
                    getCategoryResponse.setCreatedBy(category.getCreatedBy());
                    getCategoryResponse.setCreatedAt(category.getCreatedAt());
                    getCategoryResponse.setUpdatedBy(category.getUpdatedBy());
                    getCategoryResponse.setUpdatedAt(category.getUpdatedAt());
                    return getCategoryResponse;
                })
                .collect(Collectors.toList())
                ;
    }

    private Category getCategory(String categoryId) throws CategoryNotFoundException {
        return categoryRepository.findById(categoryId).orElseThrow(() -> new CategoryNotFoundException("Category ID: [" +categoryId+ "] not found"));
    }
}
