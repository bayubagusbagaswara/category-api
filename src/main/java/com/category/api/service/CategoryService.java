package com.category.api.service;

import com.category.api.dto.*;
import com.category.api.exception.CategoryNotFoundException;

import java.util.List;

public interface CategoryService {

    GetCategoryResponse createCategory(CreateCategoryRequest createCategoryRequest);

    GetCategoryResponse getCategoryById(String categoryId) throws CategoryNotFoundException;

    GetAllCategoryResponse getAllCategories(GetAllCategoryRequest getAllCategoryRequest);

    GetCategoryResponse updateCategory(String categoryId, UpdateCategoryRequest updateCategoryRequest) throws CategoryNotFoundException;

    void deleteCategory(String categoryId) throws CategoryNotFoundException;

    GetCategoryResponse getCategoryByName(String name) throws CategoryNotFoundException;

    List<GetCategoryResponse> getCategoryByNameContaining(String name);
}
