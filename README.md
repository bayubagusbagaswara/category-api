# Category API

- di category terdapat entity Category
- dimana memiliki field name category dan description category
- nantinya category ini akan berelasi dengan product
- karena 1 Category bisa memiliki banyak Product, sedangkan 1 Product hanya memiliki 1 Category
- dan nanti bisa Bidirectional, artinya jika kita ambil data product dari entitas Product, maka kita juga mendatkan data categorynya
- sebalikanya jika kita meload data Category, maka bisa dilihat juga Product apa saja yang memiliki category tersebut

## JPA Auditing & Soft Delete
- Nanti kita tambahkan fiturnya bersamaan
- JPA Auditing untuk mengetahui siapa yang CreatedBy dan UpdatedBy
- Soft Delete untuk menambahkan field Status Record, apakah nilainya ACTIVE atau INACTIVE

- consume adalah data yang dikirim atau dikonsumsi oleh API kita (request)
- produces adalah hasil yang dikembalikan dari API (response)