# API Spec

## Create Category

Request :
- Method : POST
- Endpoint : `/api/categories`
- Header :
    - Content-Type (consumes): application/json
    - Accept (produces): application/json
- Body :

```json
{
  "id": "string unique",
  "name": "string",
  "description": "string"
}
```

Response :

```json
{
  "code": "number",
  "status": "string",
  "data": {
    "id": "string unique",
    "name": "string",
    "description": "string",
    "createdBy": "string",
    "createdAt": "date",
    "updatedBy": "string",
    "updatedAt": "date"
  }
}
```

## Get Category By ID

Request :
- Method : GET
- Endpoint : `/api/categories/{categoryId}`
- Header :
    - Accept (produces): application/json
Response :

```json
{
  "code" : "number",
  "status" : "string",
  "data" : {
    "id" : "string unique",
    "name" : "string",
    "description": "string",
    "createdBy": "string",
    "createdAt": "date",
    "updatedBy": "string",
    "updatedAt": "date"
  }
}
```

## Update Category

Request :
- Method : PUT
- Endpoint : `/api/categories/{categoryId}`
- Header :
    - Content-Type: application/json
    - Accept: application/json
- Body :

```json
{
  "name": "string",
  "description": "string"
}
```

Response :

```json
{
  "code" : "number",
  "status" : "string",
  "data" : {
    "id" : "string unique",
    "name" : "string",
    "description": "string",
    "createdBy": "string",
    "createdAt": "date",
    "updatedBy": "string",
    "updatedAt": "date"
  }
}
```

## List Category

Request :
- Method : GET
- Endpoint : `/api/categories`
- Header :
    - Accept: application/json
- Query Param :
    - size : number,
    - page : number

Response :

```json
{
  "code": "number",
  "status": "string",
  "data": {
    "getSupplierResponsesList": [
      {
        "id": "string unique",
        "name": "string",
        "description": "string",
        "createdBy": "string",
        "createdAt": "date",
        "updatedBy": "string",
        "updatedAt": "date"
      },
      {
        "id": "string unique",
        "name": "string",
        "description": "string",
        "createdBy": "string",
        "createdAt": "date",
        "updatedBy": "string",
        "updatedAt": "date"
      }
    ],
    "pageNo": "number",
    "pageSize": "number",
    "totalElement": "number",
    "totalPages": "number",
    "last": "boolean"
  }
}
```

## Delete Category

Request :
- Method : DELETE
- Endpoint : `/api/categories/{categoryId}`
- Header :
    - Accept: application/json

Response :

```json
{
  "code" : "number",
  "status" : "string"
}
```

## Catatan
- produces : data balikan dari API
- consumes : data yang diterima oleh API dari client